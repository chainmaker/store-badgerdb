module chainmaker.org/chainmaker/store-badgerdb/v2

go 1.16

require (
	chainmaker.org/chainmaker/common/v2 v2.3.4
	chainmaker.org/chainmaker/protocol/v2 v2.3.5
	github.com/dgraph-io/badger/v3 v3.2103.2
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.7.0
)

replace github.com/dgraph-io/badger/v3 => chainmaker.org/third_party/badger/v3 v3.0.0
